import json
import requests
import numpy as np
import matplotlib.pyplot as plt


img_size = 32
num_symbols = 62


def vector_to_image(font_vector):
    symbol_matrix = font_vector.reshape((num_symbols, img_size, img_size))
    image = np.zeros(shape=(img_size*8, img_size*8))

    for i in range(8):
        for j in range(8):
            if i*8 + j < 62:
                image[i*img_size:(i+1)*img_size, j*img_size:(j+1)*img_size] = symbol_matrix[i*8 + j, :, :]

    return image

if __name__ == "__main__":
    headers = {"content-type": "application/json"}
    data = json.dumps({'z_sample': np.random.normal(scale=5.0, size=(1, 8)).tolist()})
    
    res = requests.post('http://127.0.0.1:8000/predict', 
                        headers=headers,
                        data=data)

    if res.status_code == 200:
        data = json.loads(res.content)
        prediction = np.array(data['prediction'])
        image = vector_to_image(prediction[0])
    
        plt.imshow(image, cmap=plt.cm.gray_r)
        plt.show()

    else:
        print(res.content)