import re
import bottle
from bottle import route, run, request, view, Bottle, template, static_file
import json
import numpy as np
from tensorflow import Graph, Session
from keras import backend
from keras.models import load_model
from PIL import Image
from base64 import b64encode

img_size = 32
num_symbols = 62

model_path = 'models/vae_decoder.h5'
model = load_model(model_path)
bottle.TEMPLATE_PATH.insert(0, 'C:/Users/fredrik.fagerholm/repos/font-generation/src/views')

def list_filter(config):
    ''' Matches a comma separated list of numbers. '''
    delimiter = config or ','
    regexp = r'.*' 

    def to_python(match):
        return list(map(float, match.split(delimiter)))

    def to_url(numbers):
        return delimiter.join(list(map(str, numbers)))

    return regexp, to_python, to_url


app = Bottle()
app.router.add_filter('list', list_filter)


def vector_to_image(font_vector):
    symbol_matrix = font_vector.reshape((num_symbols, img_size, img_size))
    image = np.zeros(shape=(img_size*8, img_size*8))

    for i in range(8):
        for j in range(8):
            if i*8 + j < 62:
                image[i*img_size:(i+1)*img_size, j*img_size:(j+1)*img_size] = symbol_matrix[i*8 + j, :, :]

    return image


@app.route('/predict/<z_sample:list>', method='GET')
def predict(z_sample):
    assert len(z_sample) == 8 
    z_sample = np.array([z_sample], dtype=float)
    
    prediction = model.predict(z_sample)

    img_data = vector_to_image(prediction[0])
    img_data = ((1.0 - img_data) * 255).astype(np.uint8)

    image = Image.fromarray(img_data)
    image.save('temp.png')
    
    #img_str = b64encode(image.tobytes())
    #return template('template', font_image=img_str)
    return static_file('temp.png', root='./', mimetype='image/png')

if __name__ == '__main__':
    app.run(host='localhost', port=8000, debug=True)
