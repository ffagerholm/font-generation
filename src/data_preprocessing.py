import os
import fnmatch
import numpy as np
import scipy as sp
from imageio import imread


def image_to_vector(infile, outfile, fmt, n_obs, dim=(128, 128)):
    """Reads image data and save as vectors.
    """
    img_width, img_height = dim
    image_dimension = img_height*img_width

    image_data = np.empty(shape=(n_obs, image_dimension), dtype='float32')
    labels = np.empty(shape=(n_obs,), dtype='int32')

    print('Reading image data from:', infile)

    index = 0
    for dirpath, dirs, files in os.walk(infile):
        for file_name in fnmatch.filter(files, fmt):
            image_path = os.path.join(dirpath, file_name)
            # read image from file
            image = imread(image_path)
            # invert image
            image = np.invert(image)
            # flatten image
            image_vector = np.array(image.ravel(), dtype='float32')
            # normalize data
            image_vector /= image_vector.max()
            # save image vector
            image_data[index, :] = image_vector

            # extract label from filename and convert to integer
            img_label = int(file_name.split('-')[0].strip('img'))
            # save label
            labels[index] = img_label

            index += 1
    print("Done.")

    print("Writing data to disk.")
    # save data
    np.savez(outfile, data=image_data, labels=labels)
    print("Done.")



if __name__ == '__main__':
    # 1016 fonts and 62 characters (digits, upper and lower case letters) 
    n_obs = 1016*62
    image_to_vector('data/raw/English/Fnt', 
                    'data/processed/character_vectors.npz', 
                    '*.png', 
                    n_obs)